import Discount from '../Helper';

const ProductList = (props) => {
    return(
        <div>
            { props.list.map((product, index) => (
                <h1 key={index}>
                    { product.name }
                    { product.discountPercentage ?
                    <Discount price={ product.price } discountPercentage={ product.discountPercentage } />
                    :
                    <span> R${ product.price } </span>
                    } 
                </h1>
            ))}
        </div>
    )
}
export default ProductList;