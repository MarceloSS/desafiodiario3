const Discount = (props) => {
    const percentage = props.price/100;

    return(
    <span style={{color:'red'}}> R${ props.price - props.discountPercentage*percentage } </span>
    )
}
export default Discount;
