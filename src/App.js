import './App.css';
import ProductList from "./Components/List/ProductList.jsx";
import { productList } from "./products"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ProductList list={ productList } />
      </header>
    </div>
  );
}

export default App;
